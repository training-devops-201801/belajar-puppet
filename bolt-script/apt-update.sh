#!/bin/bash

main(){
  install_puppet_repo
  update
  upgrade
}

install_puppet_repo(){
  wget https://apt.puppetlabs.com/puppet5-release-$(lsb_release -c -s).deb
  sudo dpkg -i puppet5-release-$(lsb_release -c -s).deb
}

update(){
  apt update
}

upgrade(){
  DEBIAN_FRONTEND=noninteractive apt upgrade -y
}

main
