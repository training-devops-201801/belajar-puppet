node 'belajar-devops' {
  class { 'apache': }             # use apache module
  apache::vhost { 'localhost':  # define vhost resource
    port    => '80',
    docroot => '/var/www/html'
  }
}
